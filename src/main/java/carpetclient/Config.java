package carpetclient;

/*
Config class for accessing mod variables.
 */
public class Config {
    public static boolean relaxedBlockPlacement = true;
    public static boolean snapAim = false;
    public static boolean accurateBlockPlacement = true;
    public static boolean controlQCrafting = true;
    public static boolean missingTools = false;
    public static boolean boundingBoxMarkers = false;
    public static boolean villageMarkers = false;
    public static boolean bucketGhostBlockFix = true;
    public static int structureBlockLimit = 32;
    public static boolean setTickRate = false;
    public static float tickRate;
    public static boolean elytraFix = false;
}
